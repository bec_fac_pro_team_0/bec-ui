package com.bec.workshop.project.students.controller;

import com.bec.workshop.project.students.domain.Grievance;
import com.bec.workshop.project.students.service.GrievanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
public class GrievanceController {
    @Autowired
    GrievanceService service;

    @RequestMapping("/submit-grievance")
    public ModelAndView save() {
        return new ModelAndView("grievance-form", "grievance", new Grievance());
    }

    @RequestMapping("/grievances")
    public String grievances(@RequestParam("status") String status, Model model) {
        model.addAttribute("grievances", service.getAll(status));
        model.addAttribute("status", status);
        return "grievances";
    }

    @PostMapping(value = "/grievances")
    public String submit(@Valid @ModelAttribute("grievance") Grievance grievances, BindingResult result) {
        if (result.hasErrors()) {
            return "error";
        }
        if(grievances.getGrievanceId() != null) {
            service.update(grievances);
        } else {
            service.save(grievances);
        }
        return "redirect:/grievances?status=pending";
    }

    @RequestMapping(value = "/approve-grievance")
    public String approve(@RequestParam("id") String id, Model model) {
        service.approve(id);
        return "redirect:/grievances?status=approved";
    }

    @RequestMapping(value = "/resolve-grievance")
    public String resolve(@RequestParam("id") String id, Model model) {
        service.resolve(id);
        return "redirect:/grievances?status=resolved";
    }

    @RequestMapping(value = "/delete-grievance")
    public String delete(@RequestParam("id") String id, Model model) {
        service.delete(id);
        return "redirect:/grievances?status=pending";
    }
}
