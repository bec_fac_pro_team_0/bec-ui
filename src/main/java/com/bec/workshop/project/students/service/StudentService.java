package com.bec.workshop.project.students.service;

import com.bec.workshop.project.students.domain.Student;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
@Slf4j
public class StudentService {
    @Autowired
    private RestTemplate restTemplate;

    @Value("${api.students}")
    private String studentsApi;

    public Student getById(String id) {
        try {
            return restTemplate.getForEntity(studentsApi + "/"+ id, Student.class).getBody();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void save(Student student) {
        ResponseEntity<String> responseEntity =
                restTemplate.exchange(studentsApi, HttpMethod.POST, new HttpEntity<>(student), String.class);
    }

    public void update(Student student) {
        ResponseEntity<String> responseEntity =
                restTemplate.exchange(studentsApi + "/" + student.getStudentId(), HttpMethod.PUT, new HttpEntity<>(student), String.class);
    }

    public List<Student> getAll() {
        return restTemplate.getForEntity(studentsApi, List.class).getBody();
    }

    public void delete(String id) {
        restTemplate.delete(studentsApi + "/" + id);
    }

}
