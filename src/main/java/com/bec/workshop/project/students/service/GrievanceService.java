package com.bec.workshop.project.students.service;

import com.bec.workshop.project.students.domain.Grievance;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
@Slf4j
public class GrievanceService {
    @Autowired
    private RestTemplate restTemplate;

    @Value("${api.grievances}")
    private String grievanceApi;

    public void save(Grievance grievance) {
        ResponseEntity<String> responseEntity =
                restTemplate.exchange(grievanceApi, HttpMethod.POST, new HttpEntity<>(grievance), String.class);
    }

    public void update(Grievance grievance) {
        ResponseEntity<String> responseEntity =
                restTemplate.exchange(grievanceApi + "/" + grievance.getGrievanceId(), HttpMethod.PUT,
                        new HttpEntity<>(grievance), String.class);
    }

    public List<Grievance> getAll(String status) {
         return restTemplate.getForEntity(grievanceApi + "?status=" + status, List.class).getBody();
    }

    public void delete(String id) {
        restTemplate.delete(grievanceApi + "/" + id);
    }

    public void approve(String id) {
        ResponseEntity<String> responseEntity =
                restTemplate.exchange(grievanceApi + "/approve/" + id, HttpMethod.PUT,
                       null, String.class);
    }

    public void resolve(String id) {
        ResponseEntity<String> responseEntity =
                restTemplate.exchange(grievanceApi + "/resolve/" + id, HttpMethod.PUT,
                        null, String.class);
    }
}
