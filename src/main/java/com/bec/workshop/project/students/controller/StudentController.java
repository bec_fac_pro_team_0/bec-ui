package com.bec.workshop.project.students.controller;

import com.bec.workshop.project.students.domain.Student;
import com.bec.workshop.project.students.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
public class StudentController {
    @Autowired
    private StudentService studentService;

    @GetMapping(value = "/students")
    public String list(Model model) {
        model.addAttribute("students", studentService.getAll());
        return "students";
    }

    @RequestMapping(value = "/students", method = RequestMethod.POST)
    public String submit(@Valid @ModelAttribute("student") Student student, BindingResult result) {
        if (result.hasErrors()) {
            return "error";
        }
        if(student.getStudentId() != null) {
            studentService.update(student);
        } else {
            studentService.save(student);
        }
        return "redirect:/students";
    }

    @GetMapping(value = "/add-student")
    public ModelAndView showForm() {
        return new ModelAndView("student-form", "student", new Student());
    }

    @RequestMapping(value = "/edit-student")
    public ModelAndView editStudent(@RequestParam("id") String id) {
        return new ModelAndView("student-form", "student", studentService.getById(id));
    }

    @RequestMapping(value = "/delete-student")
    public String delete(@RequestParam("id") String id, Model model) {
        studentService.delete(id);
        return "redirect:/students";
    }
}
