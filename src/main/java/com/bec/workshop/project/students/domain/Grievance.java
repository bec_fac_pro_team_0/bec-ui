package com.bec.workshop.project.students.domain;

public class Grievance {
    String grievanceId;
    String studentId;
    String studentName;
    String grievance;
    String grievanceRemarks;

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getGrievanceId() {
        return grievanceId;
    }

    public void setGrievanceId(String grievanceId) {
        this.grievanceId = grievanceId;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getGrievance() {
        return grievance;
    }

    public void setGrievance(String grievance) {
        this.grievance = grievance;
    }

    public String getGrievanceRemarks() {
        return grievanceRemarks;
    }

    public void setGrievanceRemarks(String grievanceRemarks) {
        this.grievanceRemarks = grievanceRemarks;
    }
}
