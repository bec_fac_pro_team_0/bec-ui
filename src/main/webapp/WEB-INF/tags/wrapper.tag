<html>
<head>
    <title>Bapatla Engineering College</title>
    <link href="webjars/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/styles.css" rel="stylesheet">
</head>
<body>
        <div class="container text-center">
            <h1><font color="#3366CC" face="Old English Text MT"> Bapatla Engineering College</font></h1>
            <p>(Autonomous)</p>
        </div>
        <div class="headerimage"></div>
        <nav class="navbar navbar-default">
          <div class="container-fluid">
            <div class="navbar-header">
              <a class="navbar-brand" href="#">Student Portal</a>
            </div>
            <ul id="topNav" class="nav navbar-nav">
              <li id="home"><a href="/home">Home</a></li>
              <li id="students-home"><a href="/students">Students</a></li>
              <li id="grievances-home"><a href="/submit-grievance">Grievance Redressal System</a></li>
              <li id="contact-home"><a href="/contact">Contact</a></li>
            </ul>
          </div>
        </nav>
        <jsp:doBody/>
        <script src="webjars/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</body>
</html>
