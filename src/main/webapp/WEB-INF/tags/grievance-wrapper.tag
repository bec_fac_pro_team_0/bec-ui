<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:wrapper>
    <div class="divThree">
        <h3>Grievance Redressal System</h3></th>
         <div class="divOne" >
             <ul id="grievance-nav" class="nav">
                 <li id="grievance-form"><a href="/submit-grievance">Grievance Form</a></li>
                 <li id="grievance-pending"><a href="/grievances?status=pending">Pending Grievances</a></li>
                 <li id="grievance-approved"><a href="/grievances?status=approved">Approved Grievances</a></li>
                 <li id="grievance-resolved"><a href="/grievances?status=resolved">Resolved Grievances</a></li>
             </ul>
         </div>
        <div class="divTwo">
           <jsp:doBody/>
        </div>
    </div>
</t:wrapper>
