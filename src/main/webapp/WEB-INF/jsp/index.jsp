<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:wrapper>
    <h1>Welcome to Bapatla Engineering College</h1>
    <script src="webjars/jquery/1.9.1/jquery.min.js"></script>
    <script>
        $(document).ready(function() {
            $("topNav li").removeClass("active")
            $("#home").addClass("active")
        });
    </script>
</t:wrapper>
