<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:wrapper>
    <div class="container text-center" style="width:500px">
        <h3>Enter the student details</h3>
        <form:form method="POST" action="/students" modelAttribute="student">
               <div class="form-group">
                   <c:if test="${not empty student.studentId}">
                        <form:label path="studentId">Student Id</form:label>
                        <form:input class="form-control" readonly="true" path="studentId"/>
                   </c:if>
               </div>
               <div class="form-group">
                    <form:label path="firstName">First Name</form:label>
                    <form:input class="form-control" path="firstName"/>
               </div>
               <div class="form-group">
                    <form:label path="lastName">Last Name</form:label>
                    <form:input class="form-control" path="lastName"/>
               </div>
               <button type="submit" class="btn btn-primary">Submit</button>
        </form:form>
    </div>
     <script src="webjars/jquery/1.9.1/jquery.min.js"></script>
     <script>
        $(document).ready(function() {
            $("topNav li").removeClass("active")
            $("#students-home").addClass("active")
        });
     </script>
</t:wrapper>
