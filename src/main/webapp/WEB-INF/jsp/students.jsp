<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:wrapper>
    <div class="container">
        <table class="table table-striped">
            <caption><h3> Students List <a style="padding-left:800" href="/add-student">Add Student</a></h3></caption>
            <thead>
                <tr>
                    <th>Student Id</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                 <c:forEach  items="${students}" var ="student">
                     <tr>
                        <td>${student.studentId}</td>
                        <td>${student.firstName}</td>
                        <td>${student.lastName}</td>
                        <td>  <a class="btn btn-default" href="<c:url value='/edit-student?id=${student.studentId}'/>"> Edit</a> </td>
                        <td>  <a class="btn btn-default" href="/delete-student?id=${student.studentId}"> Delete</a> </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </div>
    <script src="webjars/jquery/1.9.1/jquery.min.js"></script>
    <script>
        $(document).ready(function() {
            $("topNav li").removeClass("active")
            $("#students-home").addClass("active")
        });
    </script>
</t:wrapper>
