<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:grievance-wrapper>
<div class="container">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Grievance Id</th>
                    <th>Student Id</th>
                    <th>Student Name</th>
                    <th>Grievance</th>
                    <c:if test="${'resolved' == status}">
                        <th>Remarks</th>
                     </c:if>
                </tr>
            </thead>
            <tbody>
                 <c:forEach  items="${grievances}" var ="grievance">
                     <tr>
                         <td>${grievance.grievanceId}</td>
                        <td>${grievance.studentId}</td>
                        <td>${grievance.studentName}</td>
                        <td>${grievance.grievance}</td>
                         <c:if test="${'pending' == status}">
                              <td> <a class="btn btn-default" href="<c:url value='/approve-grievance?id=${grievance.grievanceId}'/>"> Approve</a> </td>
                              <td> <a class="btn btn-default" href="<c:url value='/delete-grievance?id=${grievance.grievanceId}'/>"> Delete</a> </td>
                         </c:if>
                         <c:if test="${'approved' == status}">
                            <td> <a class="btn btn-default" href="<c:url value='/resolve-grievance?id=${grievance.grievanceId}'/>"> Resolve </a> </td>
                         </c:if>
                         <c:if test="${'resolved' == status}">
                            <td>${grievance.remarks}</td>
                         </c:if>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </div>
    <script src="webjars/jquery/1.9.1/jquery.min.js"></script>
    <script>
        $(document).ready(function() {
            $("topNav li").removeClass("active")
            $("#grievances-home").addClass("active")
        });
    </script>
</t:grievance-wrapper>
