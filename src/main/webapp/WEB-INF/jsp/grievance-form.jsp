<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:grievance-wrapper>

                <form:form method="POST" action="/grievances" modelAttribute="grievance">
                    <div class="form-group">
                        <c:if test="${not empty grievance.grievanceId}">
                            <form:label path="grievanceId">Grievance Id</form:label>
                           <form:input class="form-control" readonly="true" path="grievanceId"/>
                        </c:if>
                    </div>
                    <div class="form-group">
                        <form:label path="studentId">Student Id</form:label>
                        <form:input class="form-control" path="studentId"/>
                    </div>
                    <div class="form-group">
                        <form:label path="studentName">Student Name</form:label>
                        <form:input class="form-control" path="studentName"/>
                    </div>
                    <div class="form-group">
                        <form:label path="grievance">Grievance</form:label>
                        <form:textarea class="form-control" path="grievance"/>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form:form>
    <script src="webjars/jquery/1.9.1/jquery.min.js"></script>
    <script>
        $(document).ready(function() {
            $("topNav li").removeClass("active")
            $("#grievances-home").addClass("active")
            $("#grievance-form").addClass("selected")
        });
    </script>
</t:grievance-wrapper>
